// Copyright Epic Games, Inc. All Rights Reserved.

#include "TheosOlonGameMode.h"
#include "TheosOlonPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "../Character/TheosOlonCharacter.h"

ATheosOlonGameMode::ATheosOlonGameMode()
{
	// use our custom PlayerController class
	// PlayerControllerClass = ATheosOlonPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	//Blueprint'/Game/Blueprint/Character/TopDownCharacter.TopDownCharacter'
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}