// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheosOlonGameMode.generated.h"

UCLASS(minimalapi)
class ATheosOlonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATheosOlonGameMode();
};



