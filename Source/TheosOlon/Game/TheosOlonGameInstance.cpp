// Fill out your copyright notice in the Description page of Project Settings.


#include "TheosOlonGameInstance.h"

bool UTheosOlonGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	if(WeaponInfoTable)
	{
		FWeaponInfo* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		} else
		{
			UE_LOG(LogTemp, Warning, TEXT("UTheosOlonGameInstance::GetWeaponInfoByName - Weapon Not Found - NULL"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTheosOlonGameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
	}
	return bIsFind;
}

bool UTheosOlonGameInstance::GetProjectileInfoByName(FName NameProjectile, FProjectileInfo& OutInfo)
{
	bool bIsFind = false;
	if(ProjectileInfoTable)
	{
		FProjectileInfo* ProjectileInfoRow = ProjectileInfoTable->FindRow<FProjectileInfo>(NameProjectile, "", false);
		if (ProjectileInfoRow)
		{
			bIsFind = true;
			OutInfo = *ProjectileInfoRow;
		} else
		{
			UE_LOG(LogTemp, Warning, TEXT("UTheosOlonGameInstance::GetProjectileInfoByName - Projectile Not Found - NULL"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTheosOlonGameInstance::GetProjectileInfoByName - ProjectileTable - NULL"));
	}
	return bIsFind;
}

bool UTheosOlonGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;
	
	if (DropItemInfoTable)
	{
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();
		
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			FDropItem* DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = (*DropItemInfoRow);	
				bIsFind = true;
			}
			i++;//fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}


bool UTheosOlonGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);
		if (DropItemInfoRow)
		{
			bIsFind = true;
			OutInfo = *DropItemInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}
