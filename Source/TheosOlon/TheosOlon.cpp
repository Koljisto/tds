// Copyright Epic Games, Inc. All Rights Reserved.

#include "TheosOlon.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TheosOlon, "TheosOlon" );

DEFINE_LOG_CATEGORY(LogTheosOlon)
 