// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "TheosOlon/Weapons/Projectiles/ProjectileDefault.h"
// #include "C:/Program Files/Epic Games/UE_4.25/Engine/Plugins/Marketplace/Debug/Source/Debug/Public/Log.h"
// #include "C:/Program Files/Epic Games/UE_4.25/Engine/Plugins/Marketplace/Debug/Source/Debug/Public/Debug.h"
// #include "C:/Program Files/Epic Games/UE_4.25/Engine/Plugins/Marketplace/Debug/Source/Debug/Public/DebugLogLibrarySettings.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class THEOSOLON_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void TimerExplode(float DeltaTime);
	
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;
	
	void Explode();
	
	//FTimerHandle TimerHandle_ExplodeGrenade;
	
	bool TimerEnabled = false;
	float TimerToExplode = 0.0f;
	float TimeToExplode = 3.0f;
};
