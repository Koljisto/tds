// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TheosOlon/Weapons/Projectiles/ProjectileDefault.h"
#include "ProjectileDefault_Rocket.generated.h"

/**
 * 
 */
UCLASS()
class THEOSOLON_API AProjectileDefault_Rocket : public AProjectileDefault
{
	GENERATED_BODY()
	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;
	
	void Explode();
};