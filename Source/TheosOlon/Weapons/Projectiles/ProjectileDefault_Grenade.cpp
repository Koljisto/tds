// Fill out your copyright notice in the Description page of Project Settings.
#include "ProjectileDefault_Grenade.h"
#include "TheosOlon/Weapons/Projectiles/ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// int32 DebugExplodeShow = 0;
// FAutoConsoleVariableRef CVARExplodeShow(
//     TEXT("TheosOlon.DebugExplode"),
//     DebugExplodeShow,
//     TEXT("Draw Debug for Explode"),
//     ECVF_Cheat);

// Called when the game starts or when spawned
void AProjectileDefault_Grenade::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    TimerExplode(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplode(float DeltaTime)
{
    if(TimerEnabled)
    {
        if(TimerToExplode > TimeToExplode)
        {
            //explode
            Explode();
        } else
        {
            TimerToExplode += DeltaTime;
        }
    }
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                          UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
    TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explode()
{
    // if (DebugExplodeShow)
    // {
    //     DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
    //     DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
    // }
    //TimerEnabled = false;
    if(ProjectileSetting.ExplodeFX)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
    if(ProjectileSetting.ExplodeSound)
    {
        UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}
    
    const TArray<AActor*> IgnoredActor;
    //UGameplayStatics::ApplyRadialDamage(GetWorld(),);
    UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
        ProjectileSetting.ExplodeMaxDamage,
        ProjectileSetting.ExplodeMaxDamage*0.2f,
        GetActorLocation(),
        ProjectileSetting.ProjectileMinRadiusDamage,
        ProjectileSetting.ProjectileMaxRadiusDamage,
        5,
        NULL, IgnoredActor,nullptr,nullptr);
    
    //this->Destroy();
}

