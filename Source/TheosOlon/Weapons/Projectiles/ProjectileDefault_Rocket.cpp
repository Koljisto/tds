// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Rocket.h"

#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Rocket::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Rocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileDefault_Rocket::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor,
                                                         UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
	Explode();
}

void AProjectileDefault_Rocket::ImpactProjectile()
{
	Super::ImpactProjectile();
}

void AProjectileDefault_Rocket::Explode()
{
	//if(ProjectileSetting.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	//if(ProjectileSetting.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}
    
	const TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
        ProjectileSetting.ExplodeMaxDamage,
        ProjectileSetting.ExplodeMaxDamage*0.2f,
        GetActorLocation(),
        ProjectileSetting.ProjectileMinRadiusDamage,
        ProjectileSetting.ProjectileMaxRadiusDamage,
        5,
        NULL, IgnoredActor,nullptr,nullptr);
    
	this->Destroy();
}
