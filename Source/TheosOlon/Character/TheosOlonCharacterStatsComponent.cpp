// Fill out your copyright notice in the Description page of Project Settings.


#include "TheosOlonCharacterStatsComponent.h"

void UTheosOlonCharacterStatsComponent::ReceiveDamage(float Damage)
{
	Super::ReceiveDamage(Damage);	
}

void UTheosOlonCharacterStatsComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		
		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTheosOlonCharacterStatsComponent::GetCurrentShield() const
{
	return Shield;
}

void UTheosOlonCharacterStatsComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if(Shield < 0.0f)
			Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTheosOlonCharacterStatsComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTheosOlonCharacterStatsComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{		
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTheosOlonCharacterStatsComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTheosOlonCharacterStatsComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}
