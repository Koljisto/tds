// Copyright Epic Games, Inc. All Rights Reserved.
/*
 *На данный проект ушло:
 * 24 литра молока
 * 10 пачек бекона
 * 80 банок пива
 * 4 доставки мака
 * 3 доставки суши вайт
 * 2 доставки бургеркинга
 * Достаточно (дохуя) человекочасов
 * 190 литров воды
 * 4 доширака
 * 6 кг фарша
 * около 19 пачек спагетти
 * 
 */

#include "TheosOlonCharacter.h"
#include "TheosOlon/Weapons/Guns/WeaponDefault.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "TheosOlonCharacterStatsComponent.h"
#include "TheosOlonInventoryComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TheosOlon/FuncLibrary/Types.h"
#include "TheosOlon/Game/TheosOlonGameInstance.h"
#include "TheosOlon/Character/TheosOlonInventoryComponent.h"

ATheosOlonCharacter::ATheosOlonCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTheosOlonInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTheosOlonCharacterStatsComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATheosOlonCharacter::CharDead);
	}
	
	if(InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATheosOlonCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATheosOlonCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if(CurrentCursor)
	{
		APlayerController* MyPC = Cast<APlayerController>(GetController());
		if(MyPC)
		{
			FHitResult TraceHitResult;
			MyPC->GetHitResultUnderCursor(ECC_Visibility,true,TraceHitResult);
			const FVector CursorFv = TraceHitResult.ImpactNormal;
			const FRotator CursorR = CursorFv.Rotation();
			
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	
	MovementTick(DeltaSeconds);
}

void ATheosOlonCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);
	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize,FVector());
	}
} 

void ATheosOlonCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATheosOlonCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATheosOlonCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATheosOlonCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATheosOlonCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATheosOlonCharacter::TryReloadWeapon);
	
	PlayerInputComponent->BindAction(TEXT("SwitchPreviousWeapon"), EInputEvent::IE_Released, this, &ATheosOlonCharacter::TrySwitchPreviousWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Released, this, &ATheosOlonCharacter::TrySwitchNextWeapon);
}

void ATheosOlonCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATheosOlonCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATheosOlonCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATheosOlonCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATheosOlonCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f,0.0f,0.0f), AxisX);
	AddMovementInput(FVector(0.0f,1.0f,0.0f), AxisY);
	if(MovementState == EMovementState::SprintRun_State)
	{
		const FVector MyRotationVector = FVector(AxisX, AxisY, 0.0f);
		const FRotator MyRotator = MyRotationVector.ToOrientationRotator();
		SetActorRotation(FQuat(MyRotator));
	} else
	{
		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(),0);
		if(MyController)
		{
			FHitResult ResultHit;
			MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1,true,ResultHit);
			const float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(),ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw,0.0f)));

			if(CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f,160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f,160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f,160.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f,120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}
		}
	}
}

void ATheosOlonCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if(MyWeapon)
	{
		MyWeapon->SetWeaponStateFire(bIsFiring);
	} else
	{
		UE_LOG(LogTemp, Display, TEXT("ATheosOlonCharacter::AttackCharEvent - CurrentWeapon - NULL"));
	}
}

void ATheosOlonCharacter::CharacterUpdate() const
{
	float ResSpeed = 400.0f;
    switch (MovementState)
    {
	    case EMovementState::Aim_State:
    		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
    		break;
	    case EMovementState::AimWalk_State:
    		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
    		break;
	    case EMovementState::Walk_State:
    		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
    		break;
	    case EMovementState::Run_State:
    		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
    		break;
	    case EMovementState::SprintRun_State:
    		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
    		break;
	    default:
    		break;
    }
    GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATheosOlonCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}	
	CharacterUpdate();
	//Weapon state update
	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if(MyWeapon)
	{
		MyWeapon->UpdateStateWeapon(MovementState);
	}
}

UDecalComponent* ATheosOlonCharacter::GetCursorToWorld() const
{
	return CurrentCursor;
}

AWeaponDefault* ATheosOlonCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void ATheosOlonCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTheosOlonGameInstance* myGI = Cast<UTheosOlonGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;					

					myWeapon->WeaponSetting = myWeaponInfo;

					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					
					myWeapon->ReloadTimer = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = AdditionalWeaponInfo;
					//if(InventoryComponent)
						CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					//Not Forget remove delegate on change/drop weapon
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATheosOlonCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATheosOlonCharacter::WeaponReloadEnd);

					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATheosOlonCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <=0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if(InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATheosOlonCharacter::RemoveCurrentWeapon()
{
	
}

void ATheosOlonCharacter::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)//fix reload
		{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
		}
}

void ATheosOlonCharacter::TrySwitchPreviousWeapon()
{
	UE_LOG(LogTemp, Warning, TEXT("UTheosOlonInventoryComponent::SetAdditionalInfoWeeapon - NumSlot - %d"), InventoryComponent->WeaponSlots.Num());
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1,OldIndex, OldInfo, false))
			{ }
		}
	}
}
//TODO this shit and this need put together in ONE function
void ATheosOlonCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if(CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}
			
		if (InventoryComponent)
		{			
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo,true))
			{ }
		}
	}	
}

void ATheosOlonCharacter::WeaponReloadStart(UAnimMontage* AnimReloadChar)
{
	WeaponReloadStart_BP(AnimReloadChar);
}

void ATheosOlonCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATheosOlonCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//ToDo in BP
}

void ATheosOlonCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//ToDo in BP
}

void ATheosOlonCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if(InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}

void ATheosOlonCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 Rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(Rnd) && DeadsAnim[Rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[Rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[Rnd]);
	}

	bIsAlive = false;

	UnPossessed();

	//Timer rag doll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,this, &ATheosOlonCharacter::EnableRagdoll, TimeAnim, false);	

	GetCursorToWorld()->SetVisibility(false);
}

void ATheosOlonCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}	
}

float ATheosOlonCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}
			
	return ActualDamage;
}

void ATheosOlonCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//ToDo in BP
}
