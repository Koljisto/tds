// Fill out your copyright notice in the Description page of Project Settings.


#include "TheosOlonHealthComponent.h"

// Sets default values for this component's properties
UTheosOlonHealthComponent::UTheosOlonHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTheosOlonHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTheosOlonHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTheosOlonHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTheosOlonHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTheosOlonHealthComponent::ReceiveDamage(float Damage)
{
	Health -= Damage;
	if(Health < 0.0f)
	{
		OnDead.Broadcast();
		//DeadEvent();
	}
	OnHealthChange.Broadcast(Health, Damage);
}

void UTheosOlonHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();			
		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}