// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TheosOlon/FuncLibrary/Types.h"
#include "TheosOlon/Weapons/Guns/WeaponDefault.h"

#include "TheosOlonCharacter.generated.h"

UCLASS(Blueprintable)
class ATheosOlonCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATheosOlonCharacter();
	
	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// /** Returns CursorToWorld subobject **/
	// FORCEINLINE class UDecalComponent* GetCursorToWorld() const { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	class UTheosOlonInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UTheosOlonCharacterStatsComponent* CharHealthComponent;

	// /** A decal that projects to the cursor location. */
	// UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	// class UDecalComponent* CursorToWorld;
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementSpeedInfo;	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*> DeadsAnim;

	//Weapon

	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)

	UDecalComponent* CurrentCursor = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentIndexWeapon = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;
	
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
    void InputAxisY(float Value);
	UFUNCTION()
    void InputAttackPressed();
	UFUNCTION()
    void InputAttackReleased();
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	//tick func
	void MovementTick(float DeltaTime);
	
	//func
	UFUNCTION(BlueprintCallable)
    void AttackCharEvent(bool bIsFiring);
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate() const;
	
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	
	UFUNCTION(BlueprintCallable)
    UDecalComponent* GetCursorToWorld() const;
	
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon() const;
	
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
    void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
    void TrySwitchPreviousWeapon();
	UFUNCTION(BlueprintCallable)
    void TrySwitchNextWeapon();

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* AnimReloadChar);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
	
	UFUNCTION(BlueprintNativeEvent)
    void WeaponReloadStart_BP(UAnimMontage* AnimReloadChar);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	
	UFUNCTION()
    void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
    void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION()
    void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

};